const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/IndexPage.vue") },
      { path: "fecha", component: () => import("pages/FechaTest.vue") },
    ],
  },

  {
    path: "/hello",
    component: () => import("pages/HelloWorld.vue"),
  },

  {
    path: "/hello2",
    component: () => import("layouts/MainLayout.vue"),
    children: [{ path: "", component: () => import("pages/HelloWorld.vue") }],
  },

  {
    path: "/hellocomposition",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/HelloWorldComposition.vue") },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
